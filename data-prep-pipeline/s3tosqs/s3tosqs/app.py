# This piece of code is lists all the raw video and annotation files in a given bucket/prefix
# And sends them to a processing queue (to_labeler for videos or to_framer for annotation files)
# Currently setup as a but since this is only invoked as needed it is probably better suited as a standalone script.
import json
import boto3
from boto3 import client, resource
import concurrent.futures

# SETUP LOGGING
import logging
from pythonjsonlogger import jsonlogger

LOG = logging.getLogger()
LOG.setLevel(logging.INFO)
logHandler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter()
logHandler.setFormatter(formatter)
LOG.addHandler(logHandler)


label_queue = 'to_labeler'
video_queue = "to_framer"
S3_bucket = 'sumatra-animals'
prefix = 'Sumatra-Data'


def list_s3_items(bucket, prefix):
    """Returns a list of all items in an given location of a given s3 bucket"""
    
    LOG.info(f"Generating list of items in {bucket}/{prefix}")
    
    conn = client('s3')
    # Use paginator in order to list more than 1000 objects
    paginator = conn.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket, Prefix=prefix)
    item_list = []
    for page in pages:
        for obj in page['Contents']:
            item_list.append(obj["Key"])
    videos = [item for item in item_list if item.endswith('.mp4')]
    annotations = [item for item in item_list if item.endswith('.json')]

    LOG.info(f"Total items found: {len(item_list)}. Videos: {len(videos)}. Annotations: {len(annotations)}")
    return item_list
    
def zip_item_and_dst_queue(item_list, label_queue, video_queue):
    """Returns a list of the filename zipped with its destination queue"""
    
    LOG.info(f"Zipping item list with destination queue label")
    
    mapping = []
    for item in item_list:
        if item.endswith(".mp4"):
            mapping.append((video_queue, item))
        elif item.endswith(".json"):
            mapping.append((label_queue, item))

    LOG.debug(f"Mapped list: {mapping}")
    return mapping
    
    
def send_sqs_msg(item_name, queue_name, delay=0):
    """
    Send SQS Message
    
    Expects an SQS queue_name and item_name as strings.
    Returns a response dictionary.
    """
    
    SQS = client("sqs")
    queue_url = SQS.get_queue_url(QueueName=queue_name)["QueueUrl"]
    LOG.debug(f"Send message to queue url: {queue_url}, with body: {item_name}")

    response = SQS.send_message(
        QueueUrl = queue_url,
        MessageBody = item_name,
        DelaySeconds = delay)
        
    LOG.debug("Message Response: %s for queue url: %s" % (response, queue_url))
    return response
    
    

def lambda_handler(event, context):
    item_list = list_s3_items(S3_bucket, prefix)
    mapping = zip_item_and_dst_queue(item_list, label_queue, video_queue)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        send_to_sqs_results = [executor.submit(send_sqs_msg, item_name=item_path, queue_name=queue_name) for queue_name, item_path in mapping]
    for _ in concurrent.futures.as_completed(send_to_sqs_results):
        pass
    LOG.info("Done sending filenames to sqs")
    # for queue_name, item_path in mapping:
    #     send_sqs_msg(item_path, queue_name)
    
    
if __name__=="__main__":
    lambda_handler(None,None)