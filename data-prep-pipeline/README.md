# Data Preparation Pipeline and Helper Functions

This directory contains the various tools used in preparing the all the data (videos and annotations) for modelling. Frame extraction, dataset identification, etc...

frame-extraction-batch - src code for the Elastic Container Registry repository used by AWS Batch for extracting the video into individual frames.  
labeler - src code for the Lambda function that splits the full video annotation (from Supervisely) into frame level annotations for model training.  
record-data-in-DynamoDB - src code for the Lambda function that populates the initial fields in training data database.  
s3tosqs - src code for the function that scans the raw training data in s3 and populates processing queues with those file paths.  
split-training-data - src code used for assigning train/validation/test datasets to the videos found in the training database.  
trigger-frame-extraction-batch - src code for the Lambda function used to trigger the frame extraction job within AWS Batch
