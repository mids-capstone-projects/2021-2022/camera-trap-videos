# This downloads data from the DynamoDB table containing the training data
# Opens it with pandas and runs a train-test-split stratified on the animal class
# Saves the dataset labels as a new row in the dataframe
# Updates DynamoDB with dataset info
# **Note that this script should be run AFTER the record-data-in-DynamoDB main script has finished loading the training data information into DynamoDB

import json
import pandas as pd
from sklearn.model_selection import train_test_split
import boto3
from boto3.dynamodb.conditions import Attr
import numpy as np

# Configure logging
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler
LOG, formatter = get_logger_and_logFormatter()
setup_log_stream_handler(LOG, formatter)
LOG.setLevel(20) # 20: info, 10: debug

# Here we state out target resource names
# S3_BUCKET = 'saving-nature-uploads'
DYNAMODB_TABLE = 'training-data'
# LOG.debug({'s3bucket':S3_BUCKET, 'dynamodbTable':DYNAMODB_TABLE})


# Here we assign our aws resources to use
s3_resource = boto3.resource('s3')
dynamodb_resource = boto3.resource('dynamodb')

# Here we reference our target table
table = dynamodb_resource.Table(DYNAMODB_TABLE)


def add_dataset_to_dynamo(table, video_name, mapping):
    """Adds the dataset field to the DynamoDB table"""
    response = table.update_item(
        Key={'video_name':video_name},
        UpdateExpression='SET dataset = :data',
        ExpressionAttributeValues={
            ':data': mapping[video_name]
        }
        )
    LOG.debug({"tagItem_res": response})
        
    return response
    
    
def main():
    # Grab the annotated items in the table (first 100mb)
    LOG.info("Scanning table...")
    response = table.scan(
        FilterExpression=Attr("is_annotated").eq(True)
    )
    table_data = response['Items']
    LOG.debug({"tableScan_res": response})
    
    # Paginate to retrieve the rest of the table
    while 'LastEvaluatedKey' in response:
        response = table.scan(
            FilterExpression=Attr("is_annotated").eq(True),
            ExclusiveStartKey=response['LastEvaluatedKey']
        )
        table_data.extend(response['Items'])
    
    LOG.info(f"Found {len(table_data)} files rows found")
    LOG.info(f"Generating dataset splits")
    # Created pandas dataframe from DynamoTable
    training_dataframe = pd.json_normalize(table_data)
    
    # Subset the dataframe for only the classes with sufficient videos (>=5)
    subset_training_data = training_dataframe[training_dataframe['animal_type'].map(training_dataframe['animal_type'].value_counts()) >=5]
    
    # 60-40 split for train vs validation/test
    splits = train_test_split(subset_training_data, stratify=subset_training_data['animal_type'], test_size=.4)
    
    # 50-50 split for validation vs test
    validation_splits = train_test_split(splits[1], stratify=splits[1]['animal_type'], test_size=.5)
    
    # Add dataset labels to the dataframe
    training_dataframe.loc[:, ['dataset']] = 'None'
    training_dataframe.loc[splits[0].index, ['dataset']] = 'train'
    training_dataframe.loc[validation_splits[0].index, ['dataset']] = 'validation'
    training_dataframe.loc[validation_splits[1].index, ['dataset']] = 'test'
    training_dataframe = training_dataframe.fillna(value=np.nan)
    
    # Export dataframe values to dictionaries
    dataset_dictionary = json.loads(training_dataframe.to_json())['dataset']
    videoName_dictionary = json.loads(training_dataframe.to_json())['video_name']  
    
    # Map video name to dataset
    videoName_to_dataset_mapping = {video_name:dataset_dictionary[index_1] for index_1,video_name in videoName_dictionary.items()}
    
    LOG.info("Writing datasets to DynamoDB")
    # Update DynamoDB
    for video_name in videoName_to_dataset_mapping:
        response = add_dataset_to_dynamo(table, video_name, videoName_to_dataset_mapping)
        
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            pass
        else:
            LOG.debug(f"Problem writing to DynamoDB {response}")


def lambda_handler(event, context):
    # TODO implement
    main()

if __name__=='__main__':
    main()