# pythonjsonlogger is not used because it is a PAIN to download onto lambda functions that are created without SAM
import logging
# from pythonjsonlogger import jsonlogger

def get_logger_and_logFormatter():
    LOG = logging.getLogger()
    # formatter = jsonlogger.JsonFormatter()
    formatter = None
    return LOG, formatter

def setup_log_stream_handler(logger, formatter=None):
    streamHandler = logging.StreamHandler()
    if formatter:
        streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def setup_log_file_handler(filename, logger, formatter=None):
    fileHandler = logging.FileHandler(filename)
    if formatter:
        fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)