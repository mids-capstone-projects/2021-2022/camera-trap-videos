# This piece of code is used to invoke a frame extraction job with aws batch
# It is a lambda function triggered by the to_framer queue (which is fed by the s3tosqs script)
# The body of the lambda event should contain the s3 path to a video (*not including the bucket name... that is specified elsewhere)

import boto3
import json
import botocore
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler

LOG, _ = get_logger_and_logFormatter()
setup_log_stream_handler(LOG)

def get_batch_parameters():
    """Returns a dictionary of the parameters needed for the frame-extraction batch job"""
    batch_parameters = {
        "jobName":"frame-extraction",
        "jobQueue":"saving-nature-batch",
        "jobDefinition":"frame-extraction",
        "parameters":{
            "s3videoName": None,
            "logLevel": '20'
        }
    }
    return batch_parameters
    
def update_videoName_parameter(s3videoName, params):
    params['parameters']['s3videoName'] = s3videoName
    return params
    
def submit_batch_job(parameters):
    """Takes the job parameter dicitonary and submits the job to AWS Batch"""
    batch_client = boto3.client('batch')
    response = batch_client.submit_job(
        jobName=parameters['jobName'],
        jobQueue=parameters['jobQueue'],
        jobDefinition=parameters['jobDefinition'],
        parameters=parameters['parameters'])
    return response

def parse_event(event):
    """Handles the sqs event for string type messages, returns all messages in a list
    Also deletes message from queue"""
    
    assert len(event['Records']) == 1
    receipt_handle = event['Records'][0]['receiptHandle']
    event_source_arn = event['Records'][0]['eventSourceARN']
    message = event['Records'][0]['body']
    qname = event_source_arn.split(':')[-1]
    LOG.info(f"Attemping Deleting SQS receiptHandle {receipt_handle} with queue_name {qname}")
    response = delete_sqs_msg(queue_name=qname, receipt_handle=receipt_handle)
    LOG.info(f"Delete SQS msg attempt returned with response {response}")
    
    return message
    
def delete_sqs_msg(queue_name, receipt_handle):
    """
    Deletes message from SQS queue.
    Returns a response
    """
    sqs_client = boto3.client("sqs")
    try:
        queue_url = sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']
        LOG.debug(f"Deleting msg with ReceiptHandle {receipt_handle}")
        response = sqs_client.delete_message(QueueUrl=queue_url, ReceiptHandle=receipt_handle)
    except botocore.exceptions.ClientError as error:
        LOG.exception(f"FAILURE TO DELETE SQS MSG: Queue Name {queue_name} with error {error}")
        return None
    else:
        LOG.info(f"Succesfully deleted msg {receipt_handle} from queue {queue_name}")

    return response
    
def lambda_handler(event, context):
    print(f"Lambda event triggered... event: {event}, context {context}")
    s3videoName = parse_event(event)
    params = get_batch_parameters()
    params = update_videoName_parameter(s3videoName,params)
    response = submit_batch_job(params)
    
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
  
# ### For testing    
# if __name__ == '__main__':
#     params = get_batch_parameters()
#     response = submit_batch_job(params)
