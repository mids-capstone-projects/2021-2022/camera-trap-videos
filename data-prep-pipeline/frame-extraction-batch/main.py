# When triggered this program
# 1. retrieves a messge from the specified SQS queue containing the name of a video
# 2. downloads video, extracts and writes frames to s3
# 3. deletes message from the sqs queue

import json
from boto3 import client, resource
import botocore
import os
import cv2
import time
import concurrent.futures
import sys

# SETUP LOGGING
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler

LOG, formatter = get_logger_and_logFormatter()
setup_log_stream_handler(LOG, formatter)

# Get the dataset/class splits
with open('dataset_splits.json', "r") as f:
    dataset_splits = json.load(f)

    
FETCH_BUCKET = "sumatra-animals"
SAVE_BUCKET = "sumatra-animals-small"
QUEUE_NAME = "to_framer"

def download_video_from_s3(path, bucket):
    """Downloads mp4 locally from the s3 bucket"""
    LOG.info(f"Reading video from s3://{bucket}/{path}")
    
    s3 = resource('s3')
    LOG.debug(f"Checking for tmp directory")
    if not os.path.exists('/tmp'):
        LOG.debug("/tmp/ directory not found... creating")
        os.makedirs('/tmp')
    else:
        LOG.debug("/tmp directory found")
    local_dir = "/tmp"

    filename = path.split("/")[-1]
    local_path = os.path.join(local_dir,filename)
    LOG.info(f"Downloading video to local path {local_path}")
    try:
        s3.Bucket(bucket).download_file(str(path), str(local_path))
        LOG.info(f"Video succesfully downloaded to {local_path}")
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            LOG.info("The S3 object does not exist.")
        else:
            raise
    
    return local_path


def get_video_name(path):
    """Gets the video name form the annotation file path"""
    video_name = path.split("/")[-1].split(".")[0]
    
    LOG.info(f"Video name: {video_name}")
    return video_name
    
    
def write_frame_to_s3(frame, video_name, frame_no, dataset, bucket):
    """Encodes frame as a jpeg binary and saves it to /all_frames and to appropriate dataset folder"""
    frame_name = f"{video_name}_{str(frame_no).zfill(4)}.jpg"
    
    # encode frame as jpeg binary
    LOG.debug("Attempting encode frame as jpeg binary")
    image_string = cv2.imencode('.jpg', frame)[1].tobytes()
    LOG.debug(f"Image encoded. Type: {type(image_string)}")
    
    # Generate upload keys
    LOG.debug("Generating upload keys")
    # allkey = os.path.join('all_frames','model-training', frame_name)
    dataset_key = os.path.join('model-training', dataset, frame_name)
     
    s3 = client('s3')
     
    # Write to /all_frames           
    # LOG.info(f"Writing frame to s3 as {allkey}\nImage encoding type: {type(image_string)}")
    # s3.put_object(Bucket=BUCKET, Key = allkey, Body=image_string)
    LOG.debug(f"Writing frame to s3 as {dataset_key}\nImage encoding type: {type(image_string)}")
    s3.put_object(Bucket=bucket, Key = dataset_key, Body=image_string)
    LOG.debug(f'Frame written to s3 as {dataset_key}')
    

def recieve_message(queue_name=QUEUE_NAME):
    """
    Returns a single message from the sqs queue
    """
    sqs = resource("sqs")
    queue = sqs.get_queue_by_name(QueueName=queue_name)
    message = queue.receive_messages()
    assert len(message) == 1 # make sure only one messge is recieved
    
    return message[0]

def check_dataset(video_name):
    LOG.debug(f"Idenfifying dataset for {video_name}")
    
    if video_name in dataset_splits['train']:
        LOG.info(f"Video {video_name} is in the training set.")
        return 'train'
    elif video_name in dataset_splits['valid']:
        LOG.info(f"Video {video_name} is in the validation set.")
        return 'valid'
    elif video_name in dataset_splits['test']:
        LOG.info(f"Video {video_name} is in the test set.")
        return 'test'
    else:
        LOG.info(f"Video {video_name} is not in designated for use in model training")
        return None
        
def get_frame(frame_number, local_path):
    LOG.debug(f"Extracting frame {frame_number}")
    video_capture = cv2.VideoCapture(local_path)
    video_capture.set(cv2.CAP_PROP_POS_FRAMES,frame_number)
    success, frame = video_capture.read()
    return success, frame, frame_number


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def process_video(path, fetch_bucket, save_bucket):
    local_path = download_video_from_s3(path, fetch_bucket)
    video_name = get_video_name(path)
    dataset = check_dataset(video_name)
    
    # only get frames for videos in train or valid or test sets
    if dataset:
        LOG.debug(f"Reading local video file {local_path}")

        ### Get background frames
        ## Concurrently download and save background frames in chunks of 
        
        # list all bkg frames
        LOG.info('Processing background frames...')
        frames_to_extract = dataset_splits['background'][video_name]
        num_bkg = len(frames_to_extract)
        
        # create iterable frame batches (single batch of 30)
        frame_chunks = chunker(frames_to_extract, 30)
        
        # Iterate through batches
        for batch in iter(frame_chunks):
            LOG.debug(f"Multithreading to extract and save frames for chunk. Frames: {batch}")
            start = time.perf_counter()
            with concurrent.futures.ThreadPoolExecutor() as executor:
                extract_results = [executor.submit(get_frame, frame_number=frame_no, local_path=local_path) for frame_no in batch]
                save_results = []
                for future in concurrent.futures.as_completed(extract_results):
                    success, frame, frame_no = future.result()
                    if success:
                        save_results.append(
                            executor.submit(
                                write_frame_to_s3, 
                                frame, 
                                video_name, 
                                frame_no, 
                                dataset, 
                                save_bucket
                            )
                        )
                    else:
                        print(f"Unable to save frame {frame_no}. Status {success}")
                for _ in concurrent.futures.as_completed(save_results):
                    LOG.debug("Frame saved.")
            finish = time.perf_counter()
            LOG.debug(f"Chunk processed in {round(finish-start,2)} seconds.")
        LOG.info("Done.")    
            
        ### Get annotated frames
        ## Concurrently download and save annotated frames in batches of 10
        LOG.info("Processing annotated frames...")
        # list all annotated frames
        frames_to_extract = dataset_splits[dataset][video_name][:100-num_bkg] # extract a maximum of 100 frames form a given video including bkg frames
        
        # create iiterable frame batches
        frame_chunks = chunker(frames_to_extract, 50)
        
        # iterate through batches
        for batch in iter(frame_chunks):
            LOG.debug(f"Multithreading to extract and save frames for chunk. Frames: {batch}")
            start = time.perf_counter()
            with concurrent.futures.ThreadPoolExecutor() as executor:
                extract_results = [executor.submit(get_frame, frame_number=frame_no, local_path=local_path) for frame_no in batch]
                save_results = []
                for future in concurrent.futures.as_completed(extract_results):
                    success, frame, frame_no = future.result()
                    if success:
                        save_results.append(
                            executor.submit(write_frame_to_s3, 
                                frame, 
                                video_name, 
                                frame_no, 
                                dataset, 
                                save_bucket
                            )
                        )
                for _ in concurrent.futures.as_completed(save_results):
                    LOG.debug("Frame saved.")
            finish = time.perf_counter()
            LOG.debug(f"Chunk processed in {round(finish-start,2)} seconds.")
        LOG.info("Done.")


if __name__ == "__main__":
    
    LOG.info(f'Starting processing job')

    # Recieve message (video name)
    s3_video_path = sys.argv[1]
    logLevel = int(sys.argv[2])
    LOG.setLevel(logLevel)
    LOG.info(f"S3 video path: {s3_video_path}")
    
    # Download video, and process video
    process_video(s3_video_path, FETCH_BUCKET, SAVE_BUCKET)
