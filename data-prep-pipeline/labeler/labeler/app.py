# This script created the individual frame level annotation files from the raw full video annotation files

import json
from boto3 import client, resource
import botocore
import concurrent.futures
import time
import os

FETCH_BUCKET = "sumatra-animals"
SAVE_BUCKET = "sumatra-animals-small"
QUEUE  = "to_labeler"
NUM_BACKGROUND_FRAMES = 20
TOTAL_FRAMES_TO_PROCESS = 100

# SETUP LOGGING
import logging
from pythonjsonlogger import jsonlogger

LOG = logging.getLogger()
LOG.setLevel(logging.INFO)
logHandler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter()
logHandler.setFormatter(formatter)
LOG.addHandler(logHandler)

with open("./animal_class2code.json") as f:
    class2code = json.load(f)
    
with open('dataset_splits.json', "r") as ff:
    # No longer using this file... dataset splits will be stored in the training-data dynamodb table
    dataset_splits = json.load(ff)
    
def read_json_from_s3(path, bucket):
    """Reads the json annotation file form the s3 bucket"""
    LOG.info(f"Reading annotation from {bucket}/{path}")
    
    s3 = resource('s3')
    content_object = s3.Object(bucket, path)
    file_content = content_object.get()['Body'].read().decode('utf-8')
    json_content = json.loads(file_content)
    
    LOG.info(f"Json message read succesfully: {json_content}")
    return json_content


def get_video_name(path):
    """Gets the video name form the annotation file path"""
    video_name = path.split("/")[-1].split(".")[0]
    
    LOG.info(f"Video name: {video_name}")
    return video_name


def write_json_to_s3(json_data, bucket, key):
    """Writes the new json annotation to the specified s3 bucket as model-training/{key}"""
    
    LOG.info(f"Writing to s3", extra={'json_data':json_data})
    
    s3 = client('s3')
    s3.put_object(
         Body=str(json.dumps(json_data)),
         Bucket=bucket,
         Key="model-training/"+key
    )
    
    LOG.info(f"Succesfully wrote annotation to S3 as {bucket}/model-training/{key}")
    

def delete_sqs_msg(queue_name, receipt_handle):
    """
    Deletes message from SQS queue.
    Returns a response
    """

    sqs_client = client("sqs")
    try:
        queue_url = sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']
        LOG.info(f"Deleting msg with ReceiptHandle {receipt_handle}")
        response = sqs_client.delete_message(QueueUrl=queue_url, ReceiptHandle=receipt_handle)
    except botocore.exceptions.ClientError as error:
        LOG.exception(f"FAILURE TO DELETE SQS MSG: Queue Name {queue_name} with error {error}")
        return None
    else:
        LOG.info(f"Succesfully deleted msg {receipt_handle} from queue {queue_name}")
        
    return response
    

def parse_event(event):
    """Handles the sqs event for string type messages, returns all messages in a list"""
    
    receipt_handle = event['Records'][0]['receiptHandle']
    event_source_arn = event['Records'][0]['eventSourceARN']
    
    messages = []
    # Each event contains up to 10 polled messages from sqs 
    for record in event['Records']:
        # Generate list of annotation files and Delete queue messages
        path = record['body']

        # Capture for processing
        messages.append(path)
        
        extra_logging = {"path":path}
        LOG.info(f"SQS CONSUMER LAMBDA, splitting sqs arn with value: {event_source_arn}",extra=extra_logging)
        qname = event_source_arn.split(':')[-1]
        extra_logging['qname'] = qname
        LOG.info(f"Attemping Deleting SQS receiptHandle {receipt_handle} with queue_name {qname}", extra=extra_logging)
        response = delete_sqs_msg(queue_name=qname, receipt_handle=receipt_handle)
        LOG.info(f"Delete SQS msg attempt returned with response {response}", extra=extra_logging)
    
    return messages
   
   
def check_dataset(video_name):
    """"*NO LONGER USING DATASET_SPLITS JSON
        Need to update this function to read the dataset from the training-data dynamodb table
    """
    log_msg1 = f"Checking dataset for {video_name}"
    LOG.info(log_msg1)
    
    if video_name in dataset_splits['train']:
        LOG.info(f"Video {video_name} is in the training set.")
        return 'train'
    elif video_name in dataset_splits['valid']:
        LOG.info(f"Video {video_name} is in the validation set.")
        return 'valid'
    elif video_name in dataset_splits['test']:
        LOG.info(f"Video {video_name} is in the test set.")
        return 'test'
    else:
        LOG.info(f"Video {video_name} is not in designated for use in model training")
        return None


def get_annotation_info(path, bucket):
    """
    Downloads the raw annotation from s3 and returns the information
    needed for the frames to be processed in parllel
    """
    
    # Read raw annotation from S3
    video = read_json_from_s3(path, bucket)
    
    # Get the video name
    video_name = get_video_name(path)
        
    # Sanity check that all videos share the same frame dimension
    # Videos are NOT all the same shape, fix this in the model
    height = video['size']['height']
    width = video['size']['width']
    #assert height == 720
    #assert width == 1280
        
    # There are double annotation in some of these videos
    # I will ONLY use objects identified by the most recent labeler
    objs = video['objects']
    last_labeler = objs[-1]["labelerLogin"]
    # print(f"Last labeler: {last_labeler}")
    
    #  for a given labeler, build a dictionary of their annotated objects for this  video
    class_keys = {}
    for obj in objs:
        if obj['labelerLogin'] == last_labeler:
            key = obj['key']
            class_keys[key] = obj['classTitle']
            
    dataset = check_dataset(video_name)
    
    return video, video_name, last_labeler, class_keys, dataset
    

def process_annotation_frame(frame, video_name, last_labeler, class_keys, dataset, bucket):
    """Processes a single annotation frame and saves a labels json to s3"""
    frame_number = frame['index'] 
    frame_name = f"{video_name}_{str(frame_number).zfill(4)}.json"

    # Get annotation data for the frame
    labels = []
    bbox = []
    for figure in frame['figures']:
        # Only get data from most recent labeler
        # Skip annotations by other labelers
        if figure["labelerLogin"] != last_labeler:
            continue
        
        # Get class label
        label = class_keys[figure['objectKey']]
        label_code = str(class2code[label])
        labels.append(label_code)
        
        # Get box coordinates for each bounding box
        geometry = figure['geometry']
        xmin = geometry['points']['exterior'][0][0]
        xmax = geometry['points']['exterior'][1][0]
        ymin = geometry['points']['exterior'][0][1]
        ymax = geometry['points']['exterior'][1][1]
        item = [xmin,ymin,xmax,ymax]
        bbox.append(item)

    annot = {"labels":[int(label) for label in labels],
                "boxes":bbox}

    # Write to disk
    dataset_fname = os.path.join(f"{dataset}_labels", frame_name)
    write_json_to_s3(annot, bucket, dataset_fname)


# def write_background_annotations(video, video_name, dataset, bucket):
#     LOG.debug(f"Getting background annotations for video: {video}")
#     bkg_annot = {"labels":[0],'boxes':[]} # This is not the correct format, but this is fixed in the dataset.py script when training
#     background_frames = []
#     for frame in video['frames']:
#         if len(frame['figures']) == 0: # No figure = background frame
#             frame_no = frame['index']
#             background_frames.append(frame_no)
#             frame_name = f"{video_name}_{str(frame_no).zfill(4)}.json"
#             dataset_fname = os.path.join(f"{dataset}_labels", frame_name)
#             write_json_to_s3(bkg_annot, bucket, dataset_fname)
#         else:
#             continue
#     LOG.debug(f"Done writing background frames to s3. Frames: {background_frames}")
#     return background_frames


def lambda_handler(event, context):
    LOG.info(f'SURVEYJOB LAMBDA, event {event}, context {context}')
    
    annotations = parse_event(event)
    LOG.info(f"Annotation paths recieved from sqs: {annotations}")
    
    # Generate and save annotation files (there should onlybe one path)
    for path in annotations:
        video, video_name, last_labeler, class_keys, dataset = get_annotation_info(path, FETCH_BUCKET)
        
        # Only get annotations for videos to be trained on
        if dataset:
            with concurrent.futures.ThreadPoolExecutor() as executor:
                for frame in video['frames']:
                    annotated_frame_idx = []
                    bkg_frame_idx = []
                    bkg_annot = {"labels":[0],'boxes':[]}
                    results = []
                    
                    # process and list background frames
                    if (len(frame['figures']) == 0) and (len(bkg_frame_idx) > NUM_BACKGROUND_FRAMES):
                        frame_no = frame['index']
                        LOG.debug(f"Preparing background frame annotation. Frame no.{frame_no}")
                        bkg_frame_idx.append(frame_no)
                        frame_name = f"{video_name}_{str(frame_no).zfill(4)}.json"
                        dataset_fname = os.path.join(f"{dataset}_labels", frame_name)
                        results.append(executor.submit(write_json_to_s3, json_data=bkg_annot, bucket=SAVE_BUCKET, key=dataset_fname)) 
                      
                    # process and list annotated frames  
                    elif (len(frame['figures']) > 0) and (NUM_BACKGROUND_FRAMES + len(annotated_frame_idx) > TOTAL_FRAMES_TO_PROCESS):
                        frame_no = frame['index']
                        LOG.debug(f"Preparing annotated frame annotation. Frame no.{frame_no}")
                        annotated_frame_idx.append(frame_no)
                        results.append(executor.submit(process_annotation_frame,
                                        frame=frame,
                                        video_name=video_name,
                                        last_labeler=last_labeler,
                                        class_keys=class_keys,
                                        dataset=dataset,
                                        bucket=SAVE_BUCKET))
                
                for _ in concurrent.futures.as_completed(results):
                    LOG.info(f"Annotation saved to s3")