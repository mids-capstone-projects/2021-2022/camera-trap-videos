# Lambda function that takes an annotation file and records info about that video to DynamoDB
# The function is triggered by the to_labeler sqs queue
# This script downloads the json annotation data, parses it for the video name, animal type, the last labeler, 
# indices of annotated and background frames, the frame count, and whether or not the video file is even annotated
# Writes to the specified DynamoDB table

from boto3 import resource, client
import json
import botocore
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler


LOG, formatter = get_logger_and_logFormatter()
setup_log_stream_handler(LOG, formatter)

FETCH_BUCKET = "sumatra-animals"
DYNAMODB_TABLE = "training-data"

def parse_event(event):
    """Handles the sqs event for string type messages, returns all messages in a list"""
    
    receipt_handle = event['Records'][0]['receiptHandle']
    event_source_arn = event['Records'][0]['eventSourceARN']
    
    messages = []
    # Each event contains up to 10 polled messages from sqs 
    for record in event['Records']:
        # Generate list of annotation files and Delete queue messages
        path = record['body']

        # Capture for processing
        messages.append(path)
        
        extra_logging = {"path":path}
        LOG.info(f"SQS CONSUMER LAMBDA, splitting sqs arn with value: {event_source_arn}",extra=extra_logging)
        qname = event_source_arn.split(':')[-1]
        extra_logging['qname'] = qname
        LOG.info(f"Attemping Deleting SQS receiptHandle {receipt_handle} with queue_name {qname}", extra=extra_logging)
        response = delete_sqs_msg(queue_name=qname, receipt_handle=receipt_handle)
        LOG.debug(f"Delete SQS msg attempt returned with response {response}", extra=extra_logging)
    
    return messages


def delete_sqs_msg(queue_name, receipt_handle):
    """
    Deletes message from SQS queue.
    Returns a response
    """

    sqs_client = client("sqs")
    try:
        queue_url = sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']
        LOG.debug(f"Deleting msg with ReceiptHandle {receipt_handle}")
        response = sqs_client.delete_message(QueueUrl=queue_url, ReceiptHandle=receipt_handle)
    except botocore.exceptions.ClientError as error:
        exception_msg = f"FAILURE TO DELETE SQS MSG: Queue Name {queue_name} with error {error}"
        LOG.exception(exception_msg)
        return None
    else:
        LOG.info(f"Succesfully deleted msg {receipt_handle} from queue {queue_name}")

    return response
    
    
def read_json_from_s3(path, bucket):
    """Reads the json annotation file form the s3 bucket"""
    LOG.info(f"Reading annotation from {bucket}/{path}")
    
    s3 = resource('s3')
    content_object = s3.Object(bucket, path)
    file_content = content_object.get()['Body'].read().decode('utf-8')
    json_content = json.loads(file_content)
    
    LOG.info(f"Json message read succesfully: {json_content}")
    return json_content


def get_video_name(path):
    """Gets the video name form the annotation file path"""
    video_name = path.split("/")[-1].split(".")[0]
    
    LOG.info(f"Video name: {video_name}")
    return video_name
    
    
def get_annotation_info(path, bucket):
    """
    Downloads the raw annotation from s3 and returns the information
    needed for the frames to be processed in parllel
    """
    
    # Read raw annotation from S3
    video = read_json_from_s3(path, bucket)
    
    # Get the video name
    video_name = get_video_name(path)
        
    # Sanity check that all videos share the same frame dimension
    # Videos are NOT all the same shape, fix this in the model
    height = video['size']['height']
    width = video['size']['width']
    #assert height == 720
    #assert width == 1280
        
    try:
        # There are double annotation in some of these videos
        # I will ONLY use objects identified by the most recent labeler
        objs = video['objects']
        last_labeler = objs[-1]["labelerLogin"]
        # print(f"Last labeler: {last_labeler}")
        
        #  for a given labeler, build a dictionary of their annotated objects for this  video
        class_keys = {}
        for obj in objs:
            if obj['labelerLogin'] == last_labeler:
                key = obj['key']
                class_keys[key] = obj['classTitle']
        
        annotated = True

    except IndexError as e:
        LOG.debug(f"Annotation file parse returned index error. Video likely not annotated. {e}")
        last_labeler = None
        class_keys = None
        annotated = False
        
    video_info = {
        "annotation_json": video,
        "video_name": video_name,
        "last_labeler": last_labeler,
        "class_keys": class_keys,
        "annotated": annotated
    }
    
    return video_info
    
    
def update_dynamoDB(video_info):
    dynamodb = resource('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE)
    response = table.update_item(
        Key={'video_name':video_info['video_name']},
        UpdateExpression='SET animal_type = :type, last_labeler = :lab, annotated_frames = :annot, background_frames = :bkg, frame_count = :cnt, is_annotated = :is, class_keys = :key',
        ExpressionAttributeValues={
            ':type': video_info['animal_type'],
            ':lab': video_info['last_labeler'],
            ':annot': video_info['annotated_frames'],
            ':bkg': video_info['background_frames'],
            ':cnt': video_info['annotation_json']['framesCount'],
            ':is': video_info['annotated'],
            ':key': video_info['class_keys']
        }
        )
    LOG.debug(f"Update DynamoDB table {DYNAMODB_TABLE} returned with response {response}")
    return response
    

def labeled_by_last_labeler(frame, video_info):
    """
    Checks if a given frame was labeled by the "last labeler" 
    Needed since a video can has multiple labelers, but we only want to extract frames labeled by the most recent labeler
    """
    for figure in frame['figures']:
        if figure['labelerLogin'] == video_info['last_labeler']:
            return True
    else:
        return False
    
def classify_frames(video_info):
    """Identifies the backfround frames and annotated frames from an annotation file"""
    
    annotated_frames = [frame['index'] for frame in video_info['annotation_json']['frames'] if labeled_by_last_labeler(frame, video_info)]
    total_frames = video_info['annotation_json']['framesCount']
    background_frames = [frame for frame in range(total_frames) if frame not in annotated_frames]
    extra_logging = {'background_frames':background_frames, "annotated_frames": annotated_frames}
    LOG.debug(f"Identifying background and annotated frames for video {video_info['video_name']}", extra=extra_logging)
    
    return background_frames, annotated_frames

def lambda_handler(event, context):
    """Parses a json annotation file and writes information about the video and annotation to DynamoDB"""
    # Configure logging level
    LOG.setLevel(20) # 20=info, 10=debug
    
    # Retrieve annotation file name from SQS queue and delete message from SQS
    annotations = parse_event(event)
    
    # Download and parse annotation file
    for file in annotations:
        animal_type = file.split("/")[1]
        video_info = get_annotation_info(file, FETCH_BUCKET)
        background_frames, annotated_frames = classify_frames(video_info)
        video_info['animal_type'] = animal_type
        video_info['background_frames'] = background_frames
        video_info['annotated_frames'] = annotated_frames

        # Write results to DynamoDB
        update_dynamoDB(video_info)
