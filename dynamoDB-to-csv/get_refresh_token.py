import requests

app_key = "u8i5piq1g5wy4vi"
app_secret = "iwbzv9ro3khw7rz"
refresh_url = "https://api.dropbox.com/oauth2/token"

def get_token():
    """Walks the user through creating a Dropbox refresh token for use in the web application"""
    print("Paste auth URL into browser and follow instructions to generate code")
    print("https://www.dropbox.com/oauth2/authorize?client_id=u8i5piq1g5wy4vi&token_access_type=offline&response_type=code")
    auth = input("Input the authorization code:")
    params = {
        "code": auth,
        "grant_type": "authorization_code",
        "client_id": app_key,
        "client_secret": app_secret
    }
    authorization = requests.post(refresh_url, data=params)
    print(f"Temporary access token: {authorization.json()['access_token']}")
    print(f"Refresh token: {authorization.json()['refresh_token']}")
    
if __name__ == '__main__':
    get_token()


