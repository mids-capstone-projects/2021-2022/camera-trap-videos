# requires python 3.8 runtime
# requires the pytz lambda layer arn:aws:lambda:us-east-1:770693421928:layer:Klayers-python38-pytz:6
# requires the dropbox lambda layer arn:aws:lambda:us-east-1:770693421928:layer:Klayers-p38-dropbox:1

import json
import csv
from boto3.dynamodb.conditions import Attr
from datetime import datetime
from pytz import timezone
from dropbox_utils import DropBoxUpload, get_token
import boto3, botocore
import os

GHOST_DETECTION_ONLY = True

# Configure logging
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler
LOG, formatter = get_logger_and_logFormatter()
setup_log_stream_handler(LOG, formatter)
LOG.setLevel(20) # 20: info, 10: debug

# Here we state out target resource names
S3_BUCKET = 'saving-nature-uploads-mids2022'
DYNAMODB_TABLE = 'prediction_results_and_metadata'
LOG.debug({'s3bucket':S3_BUCKET, 'dynamodbTable':DYNAMODB_TABLE})


# Here we assign our aws resources to use
s3_resource = boto3.resource('s3')
dynamodb_resource = boto3.resource('dynamodb')

# Here we reference our target table
table = dynamodb_resource.Table(DYNAMODB_TABLE)


def export_table_to_csv():
    # Here we get some timestamp attributes
    timestamps = get_timestamps()
    
    # Here we name our csv and append the date
    fileNameFormat = f'prediction-results-and-metadata_{timestamps.get("date_format")}'
    csvFileName = f'{fileNameFormat}.csv'
    LOG.info({'filename': csvFileName})
    
    # Here we setup our dynamo table and its filter expressions
    # camera_number is written to DynamoDB the moment the object is uploaded
    # animals_detected is only writted one the processing job is completed
    # Thus we only grab items in the table that have completed processing
    LOG.info("Scanning table...")
    response = table.scan(
        FilterExpression=Attr("animals_detected").exists() & Attr("camera_number").exists() & Attr('export_date').not_exists()
    )
    LOG.debug({"tableScan_res": response})
    
    if response['Count'] != 0:
        items = response['Items']
        LOG.debug(f'{len(items)} items found')
        # Here we get the keys of the first object in items.
        # We will use these keys for the headers/columns for our csv.
        keys = items[0].keys()

        # each row is a new video
        for i, row in enumerate(items):
            # write csv row
            with open(csvFileName, 'a', encoding='utf8') as f:
                dict_writer = csv.DictWriter(f, keys)
                # Here we check to see if its the first write.
                if f.tell() == 0:
                    dict_writer.writeheader()
                    dict_writer.writerow(row)
                else:
                    dict_writer.writerow(row)
                    
            # send video to dropbox
            video_name = items[i]['Long File Name']
            animals_detected = json.loads(items[i]['animals_detected'])
            ghost = items[i]['ghost']
            local_path = download_video_from_s3(video_name)
           
            if not GHOST_DETECTION_ONLY:
                if len(animals_detected) == 0:
                    animals_detected = ['ghost']
                for animal in animals_detected:
                    dropbox_path = f'/MIDS AI Data/{animal}'
                    LOG.info(f'Sending {local_path} to {dropbox_path}')
                    send_file_to_dropbox(dropbox_path,local_path)
            else:
                if ghost == 'yes':
                    dropbox_path = '/MIDS AI Data/ghost'
                    LOG.info(f'Sending {local_path} to ghost dir - {dropbox_path}')
                    send_file_to_dropbox(dropbox_path,local_path)
                elif ghost == 'no':
                    dropbox_path = '/MIDS AI Data/non-ghost'
                    LOG.info('Sending {local_path} to non-ghost dir - {dropbox_path}')
                    send_file_to_dropbox(dropbox_path,local_path)


        
        # Send the csv to dropbox
        local_path = csvFileName
        dropbox_path = '/MIDS AI Data/Result CSV Files'
        LOG.info(f"Sending {csvFileName} to DropBox in folder: {dropbox_path}")
        send_file_to_dropbox(dropbox_path, local_path)
        
        # Here we save a backup copy of the csv file in S3
        LOG.info("Writing to s3...")
        s3Object = s3_resource.Object(S3_BUCKET, f'dynamodb_exports/{csvFileName}')
        s3Response = s3Object.put(Body=open(csvFileName, 'rb'))
        LOG.info({"s3put_statusCode": s3Response['ResponseMetadata']['HTTPStatusCode']})
        LOG.debug({"s3put_res": s3Response})
        
        if s3Response['ResponseMetadata']['HTTPStatusCode'] == 200:
            # If the backup is saved succesfully add the exported date to the exported items in DynamoDB
            LOG.info("Tagging exported items...")
            response = tag_exported_items(table, items, timestamps.get("date_format"))
            return {
                "status": True,
                "tag_status": response['ResponseMetadata']['HTTPStatusCode'],
                "file_name": str(csvFileName)
            }
        else:
            return {
                "status": False
        }
    
    else:
        return {
            "status": False,
            "message": "No new items found."
        }


def get_timestamps():
    eastern = timezone('US/Eastern')
    eastern_date = datetime.now(eastern)
    fmt = '%m-%d-%Y'
    date_format = eastern_date.strftime(fmt)
    time_format = eastern_date.strftime('%I:%M:%p')

    return {
        "date_format": str(date_format),
        "time_format": str(time_format)
    }


def tag_exported_items(dynamo_table, items, tag):
    for item in items:
        response = dynamo_table.update_item(
            Key={'Long File Name':item['Long File Name']},
            UpdateExpression='SET export_date = :export',
            ExpressionAttributeValues={
                ':export': tag
            }
            )
        LOG.debug({"tagItem_res": response})
        
    return response


def send_file_to_dropbox(dropbox_path, local_path):
    token = get_token()
    default_timeout = 900
    default_chunks = 8
    
    dbu = DropBoxUpload(token, timeout=default_timeout, chunk=default_chunks)
    dbu.UpLoadFile(dropbox_path, local_path)
    
    
def download_video_from_s3(path, bucket=S3_BUCKET):
    """Downloads mp4 locally from the s3 bucket"""
    s3 = boto3.resource('s3')
    if not os.path.exists('/tmp'):
        os.makedirs('/tmp')
    local_dir = "/tmp"

    filename = path.split("/")[-1]
    local_path = os.path.join(local_dir,filename)
    try:
        s3.Bucket(bucket).download_file(str(path), str(local_path))
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            LOG.info("The object does not exist.")
        else:
            raise

    return local_path


def lambda_handler(event, context):
    LOG.debug(f"Event triggered event:{event}, context:{context}")
    response = export_table_to_csv()
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
