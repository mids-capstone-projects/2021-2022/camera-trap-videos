from diagrams import Diagram, Cluster, Edge
from diagrams.aws.storage import S3
from diagrams.aws.compute import Lambda, Batch, ECR
from diagrams.aws.database import DynamodbTable
from diagrams.aws.integration import SQS
from diagrams.aws.general import Client
from diagrams.generic.storage import Storage

attr = {
    "bgcolor": "transparent"
}
graph_attr = {
    "fontsize": "25",
}
graph_attr1 = {
    "fontsize": "20",
}

with Diagram("Web application", show=True, graph_attr=attr):
    client = Client("Website")
    table = DynamodbTable("Results and Metadata\nDatabase")
    export = Lambda("Export Results")
    dropbox = Storage("Dropbox")

    with Cluster("Backend - Object Detection", graph_attr=graph_attr):
        s3 = S3("Temporary\nStorage")
        trigger = Lambda("Trigger Batch\nProcessing")
        with Cluster("Generate Predictions",graph_attr=graph_attr1):
            batch = Batch("Batch\nProcessing")
            ecr = ECR("Containerized\nModel and\nSource Code")
            batch << ecr
        client >> Edge(label='Videos', fontsize="20") >> s3 >> trigger >> batch >> table

    client >> Edge(label='Metadata', fontsize="20") >> table >> export >> dropbox
