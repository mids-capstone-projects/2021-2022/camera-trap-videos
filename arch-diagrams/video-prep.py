from diagrams import Diagram, Cluster, Edge
from diagrams.aws.storage import S3
from diagrams.aws.compute import Lambda, Batch, ECR
from diagrams.aws.database import DynamodbTable
from diagrams.aws.integration import SQS

attr = {
    "bgcolor": "transparent"
}
graph_attr = {
    "fontsize": "25",
}

graph_attr1 = {
    "fontsize": "20",
}

with Diagram("Prepare Videos for Training", show=True, graph_attr=attr):
    s3_start = S3("Raw Data Storage")
    s3_end = S3("Formatted Data Storage")
    with Cluster("Prepare Videos for Training", graph_attr=graph_attr):
        to_framer = SQS("SQS")
        trigger_batch = Lambda("Trigger Batch\nProcessing")
        with Cluster("Extract Frames", graph_attr=graph_attr1):
            batch = Batch("Batch Processing")
            ecr = ECR("Source Code\nStorage")
            batch << ecr
        s3_start >> to_framer >> trigger_batch >> batch >> s3_end
