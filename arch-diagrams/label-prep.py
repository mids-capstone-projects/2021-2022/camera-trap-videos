from diagrams import Diagram, Cluster, Edge
from diagrams.aws.storage import S3
from diagrams.aws.compute import Lambda, Batch, ECR
from diagrams.aws.database import DynamodbTable
from diagrams.aws.integration import SQS

attr = {
    "bgcolor": "transparent"
}

graph_attr = {
    "fontsize": "25",
}

with Diagram("Prepare Labels for Training",show=True, graph_attr=attr):
    s3_start = S3("Raw Data Storage")
    s3_end = S3("Formatted Data Storage")
    with Cluster("Prepare Labels for Training", graph_attr=graph_attr):
        to_labeler = SQS("SQS")
        labeler = Lambda("Format Labels\nFunction")

        s3_start >> to_labeler >> labeler >> s3_end
