from diagrams import Diagram, Cluster, Edge
from diagrams.aws.storage import S3
from diagrams.aws.compute import Lambda, Batch, ECR
from diagrams.aws.database import DynamodbTable
from diagrams.aws.integration import SQS

attr = {
    "bgcolor": "transparent"
}
graph_attr = {
    "fontsize": "25",
}

with Diagram("Tabularize Data",show=True,graph_attr=attr):
    s3_start = S3("Raw Data Storage")

    with Cluster("Tabularize Data", graph_attr=graph_attr):

        to_tabularize = SQS("SQS")
        record_in_dynamo = Lambda("Record Data\nFunction")
        table = DynamodbTable("Training Data\nDatabase")
        split_data = Lambda("Create Data Splits\nFunction")

        s3_start >> to_tabularize >> record_in_dynamo >> table << split_data
