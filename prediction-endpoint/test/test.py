import requests
import argparse


def main(target):
    resp = requests.post(target)
    print(resp.json())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--url",default='http://localhost:5000/predict',type=str,help="Target endpoint URL")
    args = parser.parse_args()
    url = args.url
    main(url)