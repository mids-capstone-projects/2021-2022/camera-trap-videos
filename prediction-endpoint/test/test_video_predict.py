import cv2
import json
import time
import numpy as np
import requests
import argparse
from tools import send_post_request, get_pred_class_from_response, update_prediction_dictionary

    
def get_vid_preds(vid_path, endpoint):
    cap = cv2.VideoCapture(vid_path)
    i = 0
    predictions = {}
    predictions['ghost'] = 'yes'
    start = time.time()
    try:
        while(True) and (i < 2):
            # Capture frame-by-frame
            success, frame = cap.read()
            if success:
                
                # Try to predict a single frame and append results to predictions
                payload = json.dumps([str(frame.dtype), frame.tolist(), frame.shape])
                resp = send_post_request(payload, endpoint)
                print(time.time()-start)
                
                if resp.status_code == 200:
                    pred_class = get_pred_class_from_response(resp)
                    predictions = update_prediction_dictionary(i,pred_class,predictions)
                    
                else:
                    print({"error":f"error {{resp.status_code}} from prediction service"})
                
                # index the frame counter    
                i += 1
            else:
                print("Done")
                break

    except KeyboardInterrupt:
        cap.release()
        print("Stream stopped")
    return predictions
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument("--url", help="Prediction endpoint", default="http://localhost:5000/predict")
    
    args = parser.parse_args()
    target = args.url
    predictions = get_vid_preds('01240245 Mun_Mun.mp4',target)
    print(predictions)