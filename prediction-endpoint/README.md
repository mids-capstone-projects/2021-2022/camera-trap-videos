# Object Detection Prediction Endpoint

Contents
- resize.sh : a utility function for resizing an AWS Cloud9 environment (helpful when working with docker)
- Dockerfile : the configuration file for containerizing the application
- test/ : contains a empty test for validating endpoint accesibility and a video prediction test for validating application functionality
- app/ : the main application folder

