# Prediction service utilities for decoding the payload, generating class mappings, etc
import json
import numpy as np


def get_class_map():
    with open("animal_class2code.json", "r") as f:
        _map_to_code = json.load(f)
    _map_to_class = {v: k for k, v in _map_to_code.items()}
    return _map_to_class

def decodePayload(encoded_payload):
    enc = json.loads(encoded_payload)
    # build the numpy data type
    dataType = np.dtype(enc[0])
    # decode the base64 encoded numpy array data and create a new numpy array with this data & type
    dataArray = np.array(enc[1], dataType)
    # if the array had more than one data set it has to be reshaped
    if len(enc) > 2:
        dataArray.reshape(enc[2])
    return dataArray