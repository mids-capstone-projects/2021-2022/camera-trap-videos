Contents:
- utils.py : utility funtions for the application
- torch_utils.py : utility functions for the pytorch aspects of the application
- requirements.txt : packages needed for running the app
- fasterrcnn.py: contains the function for generating the model skeleton
- model_checkpoint_ep2.pt : the weights for the pytorch model
- animal_class2code.json : name to code mappings for the animal classes
- app.py: the main application file
