from flask import Flask, request, jsonify
from utils import decodePayload
from torch_utils import get_prediction

app = Flask(__name__)

@app.route('/')
def home():
    html = "<h3>Predicition Home.</h3>"
    return html.format(format)
    
    
@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        if not request.json:
            return jsonify({'error':"empty request"})
        # recieve payload
        payload = request.json['body']
        
        # decode payload
        dataArray = decodePayload(payload)
        
        # predict data
        pred_boxes, pred_class, pred_score = get_prediction(dataArray, threshold=.5, is_frame=True)

        return jsonify({'predictions': {"boxes":pred_boxes, 'class':pred_class, "score":pred_score}})
    
    else:
        return jsonify({'error':"not a post request"})
    
if __name__ == '__main__':
    app.run(host='localhost', port=8080, debug=True)