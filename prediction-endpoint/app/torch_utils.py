# Prediction service application utilities that implement pytorch
import torch
import torchvision.transforms as T
from PIL import Image
from fasterrcnn import get_object_detection_model
import numpy as np
from utils import get_class_map
from collections import OrderedDict

num_classes = 54
model_path = 'model_checkpoint_ep2.pt'

# load model
def load_eval_model(path):
    model = get_object_detection_model(num_classes)
    
    state_dict = torch.load(path, map_location=torch.device('cpu'))
    if 'model_state_dict' in state_dict:
        state_dict = state_dict['model_state_dict']

    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v

    model.load_state_dict(new_state_dict)
    model.eval()
    torch.no_grad()
    return model
    
    
model = load_eval_model(model_path)


# define transformations
def transform_image(image):
    transform = T.Compose([T.ToTensor()])
    return transform(image)
    
    
# predict
def get_prediction(img, threshold: int, is_frame: bool=True):
    """
    model: the model generating the prediciton
    img: image to be evaluated
    threshold: cutoff threshold for a valid prediciton
    is_frame: set False for loading an image from disk, True if img is a cv2 frame
    
    All values are returned as strings so to be JSON serializable
    """
    try:
        if not is_frame:
            img = Image.open(img)
        img = transform_image(img)
        pred = model([img])
        _map_to_class = get_class_map()
        pred_class = [_map_to_class[i] for i in np.array(pred[0]["labels"])]
        pred_boxes = [
            [(str(i[0]), str(i[1])), (str(i[2]), str(i[3]))] for i in np.array(pred[0]["boxes"].detach())
        ]
        pred_score = [str(x) for x in pred[0]["scores"].detach().numpy().tolist()]
        pred_t = [pred_score.index(x) for x in pred_score if float(x) > threshold][-1]
        pred_boxes = pred_boxes[: pred_t + 1]
        pred_class = pred_class[: pred_t + 1]
        pred_score = pred_score[: pred_t + 1]
        return pred_boxes, pred_class, pred_score
    except IndexError:
        print("No prediciton")
        return list(), list(), list()
        
        



    

