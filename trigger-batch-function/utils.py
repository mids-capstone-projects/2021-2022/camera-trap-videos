

def get_batch_parameters():
    batch_parameters = {
        "jobName":"video-prediction",
        "jobQueue":"saving-nature-batch",
        "jobDefinition":"video-prediction",
        "parameters":{
            "s3videoName": None,
            "logLevel": "10",
            "predThresh": ".5"
        }
    }
    return batch_parameters
    

def update_videoName_parameter(s3videoName, params):
    params['parameters']['s3videoName'] = s3videoName
    return params