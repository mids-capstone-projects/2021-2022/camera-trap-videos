import boto3
import json
from utils import get_batch_parameters, update_videoName_parameter

client = boto3.client('batch')

def submit_batch_job(parameters):
    res = client.submit_job(
        jobName=parameters['jobName'],
        jobQueue=parameters['jobQueue'],
        jobDefinition=parameters['jobDefinition'],
        parameters=parameters['parameters'])
    return res


def lambda_handler(event, context):
    print(f"Lambda event triggered... event: {event}, context {context}")
    s3videoName = event['Records'][0]['s3']['object']['key'].split('/')[-1]
    params = get_batch_parameters()
    params = update_videoName_parameter(s3videoName,params)
    response = submit_batch_job(params)
    
    return {
        'statusCode': 200,
        'body': json.dumps(response)
    }
  
### For testing    
# if __name__ == '__main__':
#     s3videoName = 'Asia_golden_cat09030092_Kucing.mp4'
#     params = get_batch_parameters()
#     params = update_videoName_parameter(s3videoName,params)
#     response = submit_batch_job(params)
