import cv2
from utils import (
            update_prediction_dictionary,
            init_prediction_dictionary, 
            download_video_from_s3, 
            update_dynamoDB
            )
from torch_utils import get_prediction, load_eval_model, get_model_path
from logging_utils import get_logger_and_logFormatter, setup_log_stream_handler
import sys


def get_video_predictions(vid_path, thresh):
    model = load_eval_model(get_model_path())
    cap = cv2.VideoCapture(vid_path)
    i = 0
    video_name = vid_path.split('/')[-1]
    predictions = init_prediction_dictionary(video_name)

    try:
        while(True):
            # Capture frame-by-frame
            success, frame = cap.read()
            if success:
                if i%50==0:
                    LOG.info(f'Predicting frame {i}')
                    
                # Try to predict a single frame and append results to predictions
                video_predicitons = get_prediction(model, frame, threshold=thresh, is_frame=True)
                predictions = update_prediction_dictionary(i, video_predicitons, predictions)

                # index the frame counter    
                i += 1
            else:
                LOG.info("End of video")
                break

    except KeyboardInterrupt:
        cap.release()
        print("Stream stopped")
        
    return predictions
    
if __name__ == '__main__':
    # What runs during the batch job...
    
    # Configure logging
    LOG, formatter = get_logger_and_logFormatter()
    setup_log_stream_handler(LOG, formatter)
    level = int(sys.argv[2]) # 20 is info, 10 is debug
    threshold = float(sys.argv[3]) # prediction confidence threshold
    
    # Start Job    
    path = sys.argv[1] # Passed as the s3videoName parameter in the Batch job submission
    LOG.setLevel(level)
    LOG.info(f"Downloading video {path}...")
    path_to_video = download_video_from_s3(path)
    LOG.info(f"Video downloaded to {path_to_video}.")
    LOG.info("Generating predictions...")
    prediction_dictionary = get_video_predictions(path_to_video, threshold)
    LOG.info("Predictions finished.")
    LOG.info(f"Writing to DynamoDB. {prediction_dictionary}")
    response = update_dynamoDB(prediction_dictionary)
    LOG.info(f"Write to DynamoDB finished with response: {response}")
