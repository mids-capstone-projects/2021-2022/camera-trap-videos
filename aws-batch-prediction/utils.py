# Prediction service utilities for decoding the payload, generating class mappings, etc
import json
import os
import boto3
import botocore


DOWNLOAD_BUCKET = 'saving-nature-uploads-mids2022'
UPLOAD_BUCKET = 'saving-nature-uploads-mids2022'
DYNAMODB_TABLE = 'prediction_results_and_metadata'

def get_class_map():
    with open("animal_class2code.json", "r", encoding='utf8') as f:
        _map_to_code = json.load(f)
    _map_to_class = {v: k for k, v in _map_to_code.items()}
    return _map_to_class


def download_video_from_s3(path, bucket=DOWNLOAD_BUCKET):
    """Downloads mp4 locally from the s3 bucket"""
    s3 = boto3.resource('s3')
    if not os.path.exists('/tmp'):
        os.makedirs('/tmp')
    local_dir = "/tmp"

    filename = path.split("/")[-1]
    local_path = os.path.join(local_dir,filename)
    try:
        s3.Bucket(bucket).download_file(str(path), str(local_path))
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

    return local_path
    

def init_prediction_dictionary(video_name):
    predictions = {}
    predictions['video_name'] = video_name
    predictions['ghost'] = 'yes'
    predictions['animals_detected'] = []
    predictions['time_first_detected'] = []
    predictions['max_animals_detected'] = 0
    predictions['time_of_max_detected'] = None
    return predictions
    
    
def update_prediction_dictionary(i, video_predictions, predictions):
    _, pred_class, pred_score = video_predictions # box, class, score each as lists
    if (len(pred_class) != 0): # is a class predicted?
        predictions['ghost'] = 'no'
    for i, predicted_class in enumerate(pred_class):
        if predicted_class not in predictions['animals_detected']:
            predictions['animals_detected'].append( (predicted_class,pred_score[i]) )
            time = 30*i//900
            predictions['time_first_detected'].append( (predicted_class,time) )
    if len(pred_class) > predictions['max_animals_detected']:
        predictions['max_animals_detected'] = len(pred_class)
        time = 30*i//900
        predictions['time_of_max_detected'] = time
        
    return predictions
    

def update_dynamoDB(predictions):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(DYNAMODB_TABLE)
    response = table.update_item(
        Key={'Long File Name':predictions['video_name']},
        UpdateExpression='SET ghost = :g, animals_detected = :a, time_first_detected = :t1',
        ExpressionAttributeValues={
            ':g': predictions['ghost'],
            ':a': json.dumps(predictions['animals_detected']),
            ':t1': json.dumps(predictions['time_first_detected'])
        }
        )
    return response

## # In case counts becomes useful in the future, below is the syntax used

##For the Update expression, add
# max_animals_detected = :m, time_of_max_detected = :tmax'

##For the Expression Attribute Values add
# ':m': json.dumps(predictions['max_animals_detected']),
# ':tmax': json.dumps(predictions['time_of_max_detected'])

