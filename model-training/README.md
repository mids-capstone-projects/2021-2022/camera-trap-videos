# Saving Nature Model Training

The contents of this directory are all thats needed in the /src/ directory referenced in the Sagemaker training job.

train.py - defines the training script *the evalutation section has been commented out because it was crashing training jobs*.  
transforms.py - defines alternative versions of functions found in torchvision.transforms which are needeed to run the training script.  
dataset.py - defines the PyTorch implementation of the training/testing/validaiton dataset.  
fasterrcnn.py - defines the backbone of the PyTorch implementation of the faster-rcnn model we train.  
requirements.txt - defines the python requirements needed to train the model.  
Start Training Job.ipynb - this is the notebook used within Sagemaker to initiate a training job.  
