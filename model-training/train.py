import argparse
import os
import json
import sys

import torch.distributed as dist
import torch.utils.data.distributed
import torch.utils.data
import torch.multiprocessing as mp

# Import modules for sagemaker distributed data parallel
# import smdistributed.dataparallel.torch.distributed as dist
# from smdistributed.dataparallel.torch.parallel.distributed import DistributedDataParallel as DDP

import transforms as T
import torch
import torch.optim as optim
import time
from tqdm import tqdm
from dataset import CustomDataset
from fasterrcnn import get_object_detection_model


# SETUP LOGGING
import logging
from pythonjsonlogger import jsonlogger

# LOG = logging.getLogger()
# logHandler = logging.StreamHandler(sys.stdout)
# formatter = jsonlogger.JsonFormatter()
# logHandler.setFormatter(formatter)
# LOG.addHandler(logHandler) 

def get_logger_and_logFormatter():
    LOG = logging.getLogger()
    formatter = jsonlogger.JsonFormatter()
    return LOG, formatter

def setup_log_stream_handler(logger, formatter):
    streamHandler = logging.StreamHandler(sys.stdout)
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

def setup_log_file_handler(filename, logger, formatter):
    fileHandler = logging.FileHandler(filename)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)
    
prediction_logger, formatter = get_logger_and_logFormatter()



def get_transform(train):
    """Train/Test transforms"""
    transforms = []
    transforms.append(T.ToTensor())
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


def collate_fn(batch):
    """Collate funciton for data loaders"""
    batch = list(filter(lambda x: x is not None, batch))
    return tuple(zip(*batch))


def save_checkpoint(model, optimizer, epoch, train_loss, eval_metrics, model_dir):
    """Saves model state dict"""
    print("[INFO]Saving model...")
    checkpoint_path = os.path.join(model_dir, f"model_checkpoint_ep{epoch}.pt")
    model_path = os.path.join(model_dir, f"full_model_ep{epoch}.pt")
    try:
        torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'train_loss': train_loss,
                'test_precision': eval_metrics['test_precision'],
                'test_recall': eval_metrics['test_recall'],
                'test_f1': eval_metrics['test_f1']
                }, checkpoint_path)
        print("Checkpoint saved")
    except:
        print("Error saving model history")
    finally:
        torch.save(model.state_dict(), model_path)
        print("Full model checkpoint saved")



def average_gradients(model):
    # Gradient averaging for distributed cpu training
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.reduce_op.SUM)
        param.grad.data /= size


def get_train_data_loader(args, is_distributed, rank):
    """Builds train loader - uses train_sampler if distributed"""
    print("[INFO]Preparing training data")

    img_root_train   = os.path.join(args.data_dir, "train")
    label_root_train = os.path.join(args.data_dir, "train_labels")
    transforms_train = get_transform(train=True)
    trainset         = CustomDataset(img_root=img_root_train,
                                     label_root=label_root_train,
                                     transforms=transforms_train)
    train_sampler    = torch.utils.data.distributed.DistributedSampler(trainset,
                                                                       num_replicas=args.world_size,
                                                                       rank=rank) if is_distributed else None
    trainloader      = torch.utils.data.DataLoader(trainset,
                                                   batch_size=args.batch_size,
                                                   shuffle=train_sampler is None,
                                                   sampler=train_sampler,
                                                   num_workers=args.num_workers,
                                                   collate_fn=collate_fn)
    print(f"[INFO]Training on {len(trainset)} images. {len(trainloader)} training batches")
    return trainloader


def get_test_data_loader(args, validation=True):
    """Builds test loader"""
    print("[INFO]Prepating test/validation data")

    if validation:
        img_root_valid   = os.path.join(args.data_dir, "validation")
        label_root_valid = os.path.join(args.data_dir, "validation_labels")
    else:
        img_root_valid   = os.path.join(args.data_dir, "test")
        label_root_valid = os.path.join(args.data_dir, "test_labels")

    transforms_valid = get_transform(train=False)
    valset           = CustomDataset(img_root=img_root_valid, label_root=label_root_valid, transforms=transforms_valid)
    valloader        = torch.utils.data.DataLoader(valset,
                                                   batch_size=args.test_batch_size,
                                                   shuffle=False,
                                                   num_workers=args.num_workers//args.world_size,
                                                   collate_fn=collate_fn)

    print(f"[INFO]Evaluating on {len(valset)} images. {len(valloader)} training batches")
    return valloader


def get_test_loss(model, inputs, targets):
    """Computes the overall loss - expects model an inputs on same device"""
    model.train()
    loss_dict = model(inputs, targets)
    losses    = sum(loss for loss in loss_dict.values())
    model.eval()
    return losses



def train(rank, args):
    """Main training funciton"""
    if not rank:
        rank = 0
    global_rank = False
    host_rank = 0

    print(f"[INFO]Training with\n\tEpochs: {args.epochs}\n\tBatch size: {args.batch_size}\n\tLearning Rate: {args.lr}\n\tRegularization: {args.reg}\n\tCutoff Threshold: {args.thresh}\n\tLogging every {args.log_n} batches")

    is_distributed = (len(args.hosts) > 1 and args.backend is not None) or args.num_gpus > 1
    print(f"[INFO]Distributed training: {is_distributed}")
    use_cuda = args.num_gpus > 0
    print(f"[INFO]Number of gpus available: {args.world_size} across {len(args.hosts)} node(s).")

    if is_distributed:
        # Init distributed environment
        os.environ['WORLD_SIZE'] = str(args.world_size)
        host_rank = args.hosts.index(args.current_host)
        global_rank = host_rank * args.num_gpus + rank
        local_rank = rank
        os.environ['RANK'] = str(global_rank)
        dist.init_process_group(backend=args.backend,
                                rank=global_rank,
                                world_size=args.world_size)
        print(f"[INFO]Initialized distributed environment {args.backend} backend on {dist.get_world_size()} nodes.")
        print(f"[INFO]Host {host_rank}. Local rank {local_rank}. Global rank {dist.get_rank()}.")

    if not global_rank:
        global_rank = rank

    # set the seed for equal initializations across gpus
    torch.manual_seed(args.seed)
    if use_cuda:
        torch.cuda.manual_seed(args.seed)

    print('==>Loading model..')
    net = get_object_detection_model(num_classes=54)

    if not args.device:
        device = torch.device(f"cuda:{global_rank}" if use_cuda else "cpu")
    else:
        device = args.device

    net.to(device)
    print(f"[INFO]Running on {device}")
    if is_distributed and use_cuda:
        # for multi-gpu training
        net = torch.nn.parallel.DistributedDataParallel(net, device_ids=[global_rank], output_device=global_rank)


    print('[INFO]==> Preparing data..')
    trainloader = get_train_data_loader(args, is_distributed, rank=global_rank)
    valloader = get_test_data_loader(args)

    # construct an optimizer
    params    = [p for p in net.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=args.lr, momentum=args.momentum, weight_decay=args.reg)
    scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[int(args.epochs*0.5), int(args.epochs*0.75)], gamma=0.1)

    start_epoch  = 0  # start from epoch 0 or last checkpoint epoch
    global_steps = 0
    loss_tracker = []

    start = time.time()
    for epoch in range(start_epoch, args.epochs):
        """
        Start the training code.
        """
        print(f'[INFO]Epoch: {epoch}')
        net.train()
        train_loss = 0
        for batch_idx, (inputs, targets) in enumerate(tqdm(trainloader)):
            # Move inputs images and targets to device
            inputs  = [inpt.to(device) for inpt in inputs]
            targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
            print(f"[DEBUG]Inputs moved to gpu {device}")

            # Zero gradients and compute loss
            optimizer.zero_grad()
            loss_dict = net(inputs, targets)
            del inputs
            del targets
            print("[DEBUG]Memory freed...")
            
            losses    = sum(loss for loss in loss_dict.values())
            print(f"[DEBUG]Losses computed = {losses: .4f}: GPU {device}")

            # Backprop and optimization step
            losses.backward()
            print(f"[DEBUG]Backprop complete: GPU {device}")

            if is_distributed and not use_cuda:
                average_gradients(net)
            optimizer.step()
            print(f"[DEBUG]Gradient step taken: GPU {device}")

            train_loss   += losses.item()
            del losses
            print("[DEBUG]Memory freed...")
            
            global_steps += 1

            if global_steps % args.log_n == 0:
                end = time.time()
                num_examples_per_second = args.log_n * args.batch_size / (end - start)
                loss_dict_items = list(loss_dict.items())
                total_train_loss = train_loss/(batch_idx + 1)
                print(f"""[METRICS]GPU:{global_rank}[Step={global_steps}]
                            Train loss={total_train_loss: .4f}
                            {loss_dict_items[0][0]}={loss_dict_items[0][1].item(): .4f}
                            {loss_dict_items[1][0]}={loss_dict_items[1][1].item(): .4f}
                            {loss_dict_items[2][0]}={loss_dict_items[2][1].item(): .4f}
                            {loss_dict_items[3][0]}={loss_dict_items[3][1].item(): .4f}
                            Throughput={num_examples_per_second: .1f} examples/second""")
                if global_rank == 0:
                    print(f"[INFO]Host {{{host_rank}}} speed: approx.{num_examples_per_second*args.num_gpus: .1f} examples/second")
                start = time.time()
            print(f"[DEBUG]Batch {batch_idx} finished: GPU {device}")

        scheduler.step()
        if (epoch == 0) and (global_rank == 0):
            save_checkpoint(net, optimizer, epoch, total_train_loss, None,  args.model_dir)    
        if (epoch % 5 == 0) and (global_rank == 0):
            save_checkpoint(net, optimizer, epoch, total_train_loss, None,  args.model_dir)

#         if (global_rank == 0):
#             """
#             Start the testing code.
#             """
#             net.eval()
#             eval_start = time.time()
#             print("[INFO]Evaluating...")

#             total_actual_instances    = 0
#             total_predicted_instances = 0
#             total_correct             = 0
#             best_f1                   = 0
# #             test_loss                 = 0
#             with torch.no_grad():
#                 for batch_idx, (inputs, targets) in enumerate(tqdm(valloader)):
#                     inputs      = [inp.to(device) for inp in inputs]
#                     targets     = [{k: v.to(device) for k, v in t.items()} for t in targets]
#                     print(f"[DEBUG]Eval inputs moved to gpu {device}")

#                     predictions = net(inputs)
#                     del inputs
                    
#                     with open(args.log_path, 'a') as f:
#                         print({'targets':targets, 'predicitons':predictions}, file=f)
#                     print(f"[DEBUG]Predictions generated for batch {batch_idx}/{len(valloader)}")
# #                     loss = get_test_loss(net, inputs, targets)
# #                     print(f"[DEBUG]Loss calculated {loss: .4f} for batch {batch_idx}/{len(valloader)}")


#                     # get true and predicted labels
#                     true_labels = [t['labels'] for t in targets]
#                     pred_labels = [p['labels'] for p in predictions]

#                     # create mask based on prediciton threshold
#                     pred_mask = [torch.tensor([score.item() >= args.thresh for score in p['scores']]).to(device) for p in predictions]

#                     # retrieve only masked predictions
#                     preds = [(m*p)[torch.nonzero(m*p)] for m,p in zip(pred_mask, pred_labels)]

#                     ## Compute metrics
#                     num_predicted_instances = sum([pred.shape[0] for pred in preds])
#                     num_actual_instances    = sum([labels.shape[0] for labels in true_labels])
#                     num_correct_predictions = sum([pred.eq(label).sum().item() for pred, label in zip(preds, true_labels)])

#                     # precision = num_correct_predictions / num_predicted_instances
#                     # recall    = num_correct_predictions / num_actual_instances
#                     # f1        = (2*precision*recall)/(precision + recall)

#                     # Add to running totals
#                     total_actual_instances    += num_actual_instances
#                     total_predicted_instances += num_predicted_instances
#                     total_correct             += num_correct_predictions
# #                     test_loss                 += loss
#                     print(f"[DEBUG]Batch{batch_idx} Total actual instances={num_actual_instances}, Total predicted={num_predicted_instances}, Total correct={num_correct_predictions}")
    


#             num_val_steps = len(valloader)
#             eval_end      = time.time()
#             num_examples_per_second = num_val_steps*args.test_batch_size / (eval_end - eval_start)
#             # Compute overall validation metrics
#             try:
#                 val_precision = total_correct / total_predicted_instances
#             except ZeroDivisionError:
#                 val_precision = 0.
#             val_recall = total_correct / total_actual_instances
#             print(f"[METRICS]Evaluating {num_examples_per_second: .1f} samples/second")
#             print(f"[INFO]Total predictions made={total_predicted_instances}")
#             if total_correct > 0:
#                 val_f1 = (2*val_precision*val_recall)/(val_precision + val_recall)
#                 eval_metrics = dict(test_precision=val_precision, test_recall=val_recall, test_f1=val_f1)
#                 print(f"[METRICS]Test precision={val_precision: .4f}, Test recall={val_recall: .4f}, Test f1={val_f1: .4f}")
# #                 print(f"[METRICS]Test loss={test_loss/(batch_idx + 1): .4f}, Test precision={val_precision: .4f}, Test recall={val_recall: .4f}, Test f1={val_f1: .4f}")
#                 if round(val_f1,4) > round(best_f1,4):
#                     best_f1 = val_f1
#                     save_checkpoint(net, optimizer, epoch, total_train_loss, eval_metrics,  args.model_dir)

#             else:
#                 print(f"[INFO]Test precision={val_precision: .4f}, Test recall={val_recall: .4f}")
    
    save_checkpoint(net, optimizer, epoch='final', train_loss=total_train_loss, eval_metrics=None, model_dir=args.model_dir)



if __name__=="__main__":
    print("Training Job Starting...")
    
    my_parser = argparse.ArgumentParser()

    # training paraeters
    my_parser.add_argument(
        '--epochs', metavar='epochs', type=int, default=10, help='training epochs [default=10]'
    )
    my_parser.add_argument(
        '--batch-size', metavar='batch', type=int, default=8, help='batch size [default=8]'
    )
    my_parser.add_argument(
        '--test-batch-size', metavar='test-batch', type=int, default=8, help='test batch size [default=8]'
    )
    my_parser.add_argument(
        '--lr', metavar='lr', type=float, default=0.001, help='initial learning rate [default=0.001]'
    )
    my_parser.add_argument(
        '--reg', metavar='reg', type=float, default=0.0001, help='regularization parameter [default=0.0001]'
    )
    my_parser.add_argument(
        '--thresh', metavar='thresh', type=float, default=0.5, help='cutoff score for positive predictions [default=0.3]'
    )
    my_parser.add_argument(
        '--momentum', type=float, default=0.9, metavar="M", help="SGD momentum [default=0.9]"
    )
    my_parser.add_argument(
        '--log-n', metavar='log_n', type=int, default=50, help='log every n epochs [default=50]'
    )
    my_parser.add_argument(
        '--seed', type=int, default=1, metavar="S", help="random seed [default=1]"
    )
    my_parser.add_argument(
        '--device', metavar='device', type=str, default=None, help='training device (cpu or cuda)'
    )
    my_parser.add_argument(
        '--backend', metavar='backend', type=str, default=None, help='backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)'
    )
    my_parser.add_argument(
        '--num-workers', metavar='num-workers', type=int, default=2, help='num train/test loader workers [default=2]'
    )
    my_parser.add_argument(
        '--log-level', metavar='log-level', type=int, default=20, help='log level [default=20 (info)]'
    )
    
    # container environment
    my_parser.add_argument("--hosts", type=list, default=json.loads(os.environ["SM_HOSTS"]))
    my_parser.add_argument("--current-host", type=str, default=os.environ["SM_CURRENT_HOST"])
    my_parser.add_argument("--model-dir", type=str, default=os.environ["SM_MODEL_DIR"])
    my_parser.add_argument("--data-dir", type=str, default=os.environ["SM_CHANNEL_TRAINING"])
    my_parser.add_argument("--num-gpus", type=int, default=os.environ["SM_NUM_GPUS"])
    my_parser.add_argument("--block", type=bool, default=False)
    my_parser.add_argument("--log-path", type=str, default=os.path.join(os.environ["SM_MODEL_DIR"],"prediction_logs.txt"))
    
    os.environ['PYTHONUNBUFFERED'] = "1"
    print("Running with ['PYTHONUNBUFFERED'] = '1'")


    args = my_parser.parse_args()
    
#     setup_log_file_handler(args.log_path, prediction_logger, formatter)   
#     prediction_logger.setLevel(args.log_level)

    args.world_size = args.num_gpus * len(args.hosts)
    args.batch_size //= args.world_size
#     os.environ['MASTER_ADDR'] = '10.57.23.164'
#     os.environ['MASTER_PORT'] = '8888'
#     mp.spawn(train, nprocs=args.num_gpus, args=(args,))

    # spawn N processes for N global # of gpus
    mp.spawn(train,
             nprocs=args.world_size,
             args=(args,),
             join=True)

#     if args.block:
#         os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
#         print("Running with ['CUDA_LAUNCH_BLOCKING'] = '1'")

#     train(args)
