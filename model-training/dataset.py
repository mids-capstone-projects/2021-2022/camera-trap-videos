import os
import numpy as np
import torch
from PIL import Image
import json


def read_target(label):
    with open(label, "r") as f:
        label = json.load(f)
    return label 


class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, img_root, label_root, transforms):
        self.img_root = img_root
        self.label_root = label_root
        self.transforms = transforms
        # load all image files, sorting them to
        # ensure that they are aligned
        self.imgs = list(sorted(os.listdir(img_root)))
        #self.labels = list(sorted(os.listdir(os.path.join(root, "labels"))))

    def __getitem__(self, idx):
        """Gets image by index then fetches label"""
        try:
            # load images and labels
            img_path = os.path.join(self.img_root, self.imgs[idx])
            img = Image.open(img_path).convert("RGB")
            # swapping .jpg extension with .json
            # img_dir/file.jpg -> file.json
            label_name = img_path.split("/")[-1].split(".")[0] + ".json"
            label_path = os.path.join(self.label_root, label_name)
            label = read_target(label_path)

            # get bounding label and box coordinates for each image
            labels = label["labels"]
            boxes = label['boxes']

            # convert everything into a torch.Tensor
            labels = torch.as_tensor(labels, dtype=torch.int64)
            boxes = torch.as_tensor(boxes, dtype=torch.float32)

            # fix background boxes -- [0,0,0,0] instead of []
            if boxes.shape == torch.Size([0]):
              # boxes = torch.zeros((1, 4), dtype=torch.float32)
              boxes = torch.empty((0, 4), dtype=torch.float32)

            image_id = torch.tensor([idx])
            area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])

            # build target
            target = {}
            target["boxes"] = boxes
            target["labels"] = labels
            target["image_id"] = image_id
            target["area"] = area

            if self.transforms is not None:
                img, target = self.transforms(img, target)
        except:
            return None

        return img, target

    def __len__(self):
        return len(self.imgs)
